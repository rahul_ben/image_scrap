var passport = require('passport');
var expressSession = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var flash    = require('connect-flash');
var uuid = require('uuid');
var config = require('./config');
var express = require('express');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session      = require('express-session');

module.exports.configure = function(app, passport) {
    app.use(morgan('dev')); // log every request to the console
    app.use(cookieParser()); // read cookies (needed for auth)
    // app.use(bodyParser( {limit: '900mb'} )); // get information from html forms
    // app.use(bodyParser.urlencoded({
    //     extended: true,
    //     limit: '900mb'
    // }));
    app.use(bodyParser.json({limit: '500mb'}))
    app.use(bodyParser.urlencoded({extended: true, limit: '500mb'}));
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        next();
    });
    var root = path.normalize(__dirname + './../');
    config.root = root;
    app.set('views', path.join(root, 'views'));
    app.set('view engine', 'ejs');
    app.use(express.static(path.join(root, 'public')));

   // required for passport
    app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions
    app.use(flash()); // use connect-flash for flash messages stored in session

};
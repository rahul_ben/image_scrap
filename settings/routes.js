var auth  = require('./../middleware/authorization');
var userController = require('./../controllers/users');
var quoteController = require('./../controllers/quotes');
var topicController = require('./../controllers/topic');



var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');


module.exports.configure = function(app, passport) {
    app.get('/login', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage') }); 
        
    });
    app.get('/signup', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });
    app.get('/forgetPassword', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('forgetPassword.ejs', { message: req.flash('signupMessage')  });
    });
    app.get('/newPassword', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('newPassword', { email: req.query.email  });
    });
    app.get('/activateAccount', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('activateAccount');
    });

    app.post('/sendVerificationLink', userController.sendVerificationLink)




    app.post('/newPassword', userController.newPassword)
    








     app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/activateAccount', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

	app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    app.get('/', auth.requireLogin, function (req, res) {
        if(req.user.role == "admin") {
            res.render('adminIndex', {
                currentUser: JSON.stringify({
                    token: req.user.token
                })
            });
        } else {
            res.render('index', {
                currentUser: JSON.stringify({
                    token: req.user.token
                })
            });
        }
        
     });


/* ======================================APIS======================================================*/

/*================quotes=================*/

app.post('/quote', quoteController.create);
app.get('/quote', quoteController.all);

/*================quotes=================*/

app.post('/topic', topicController.create);
app.get('/topic', topicController.all);



/*================quotes=================*/

app.post('/upload', userController.upload);
app.get('/api/users', userController.getUsers);


app.get('/scrape', function(req, res){

    // url = 'https://www.brainyquote.com/quotes/topics/topic_motivational.html';
    // url = 'https://www.brainyquote.com/quotes/topics/topic_travel.html';
    // url = 'https://www.brainyquote.com/quotes/topics/topic_life.html';
    url = 'https://www.brainyquote.com/quotes/topics/topic_romantic.html';
    // url = 'https://www.brainyquote.com/quotes/topics/topic_fitness.html?page=2';
var c = 0;
    request(url, function(error, response, html){
        if(!error){
            var $ = cheerio.load(html);

            var title, release, rating;
            var json = { title : "", release : "", rating : ""};
            var list = [];
            $('div[id="quotesList"]').find('div > div > div').each(function (index, element) {
                
                if(element.children.length > 0) {
                    c=c+1;
                console.log(c+1);
                    element.children.forEach(function(item) {
                        if(item.attribs) {
                            if(item.attribs.title == "view quote") {
                                console.log(item.children[0].data);
                                list.push(item.children[0].data);
                            }
                        }
                    })
                }
            })
            res.json(list);

        
        }
    })
})








};
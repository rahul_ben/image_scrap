var fs = require('fs');
var join = require('path').join;
var uriUtil = require('mongodb-uri');
var config = require('./config');

module.exports.configure = function(mongoose) {
	var connect = function () {
		var options = { server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } } };
		var mongooseUri = uriUtil.formatMongoose(config.dbServer.host);
		mongoose.connect(mongooseUri);
	};
	connect();
	var db = mongoose.connection;
	db.on('error', console.log);
	db.on('disconnected',connect);

	fs.readdirSync(join(__dirname, '../models')).forEach(function (file) {
		if (~file.indexOf('.js')) require(join(__dirname, '../models', file));
	});
};
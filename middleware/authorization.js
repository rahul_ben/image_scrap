var jwt = require('jsonwebtoken');
var models = require('mongoose').models;
var config = require('../settings/config');



exports.requiresToken = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(!token) {
        return res.status(403).send({
            success: false,
            message: 'Token is required.'
        });
    }
    jwt.verify(token, config.auth.secret, {
            ignoreExpiration: true
        }
        ,function(err, claims) {
        if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {  
            req.userData = claims;
            next();
        }
    });
};

exports.getToken = function(email, username) {
    var claims = {
        email: email,
        username: username
    };

    return jwt.sign(claims, config.auth.secret, { expiresIn: '1h' });
};

exports.requireLogin = function(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/login');
};
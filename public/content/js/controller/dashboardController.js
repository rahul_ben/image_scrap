var app = angular.module('app')
	.controller('dashboardController', ['$scope','$http', 'dataService', '$rootScope', 'fileUpload', function($scope, $http, dataService,$rootScope, fileUpload) {
		
		
		angular.element(document).ready(function () {
			var theHandle = document.getElementById("handle");
			var theRoot = document.getElementById("root");
			$scope.opacityFlag = false;
			$('#handle').draggable({
					helper: 'clone'
			});
			$('#contrastSlider').change(function(e) {
					var value = $(e.target).val();
					$('#image56').css('opacity', value);
			});
			$("#myrange").change(function(e) {
				$("#image56").css("opacity",this.value);
			});
			$(".dropdown-button").dropdown({
					hover: false
			});

			// document.getElementById("item-0").addEventListener("dragstart", function(e) {
			// 	var crt = this.cloneNode(true);
			// 	document.body.appendChild(crt);
			// 	e.dataTransfer.setDragImage(crt, 0, 0);
			// }, false);

			
			
			
		});


		$scope.changeRange = function() {
			$("#image56").css("opacity", 100/this.value);
		};	
		$scope.changeFontStyle = function(fontStyle) {
				document.getElementById("text").style.fontFamily = fontStyle;
		}
		$scope.changeFontSize = function(fontSize) {
				document.getElementById("text").style['font-size'] = fontSize;
		}

		$scope.uploadedImageFlag = false;
		$scope.imageUrl = "";
		$scope.loader = false;
		$scope.quotesFlag = false;
		var topicId;
		var page= 1;




		$scope.export = function() {
				$scope.loader = true;
				domtoimage.toBlob(document.getElementById('exp'))
				.then(function (blob) {			
						window.saveAs(blob, 'my-node.jpg');
				});
				$scope.loader = false;
		}
		dataService.getTopics(function(response) {
			$scope.topics = response.data;
		});


		
		$scope.getQuotes = function(topic, currentPage) {
			$scope.quotesFlag =true;
			topicId = topic;
			
			dataService.getQuotes(topic, currentPage,  function(response) {
				$scope.quotes = response.data;
				page = currentPage;
				setTimeout(function() {
					document.getElementById("item-0").addEventListener("dragstart", function(e) {
							var crt = this.cloneNode(true);
							document.body.appendChild(crt);
							e.dataTransfer.setDragImage(crt, 0, 0);
					}, false);
				}, 1000)
				
			});
		};

		$scope.refreshQuote = function() {
			page = page +1;
			$scope.getQuotes (topicId, page) 
		};

		$scope.uploadImage = function() {
			$scope.loader = true;

			setTimeout(function () {
				$scope.$apply(function () {
					var file = $scope.myFile;
					var uploadUrl = '/upload';
					fileUpload.uploadFileToUrl(file, uploadUrl, function(response) {
					$scope.loader = false;
						if(response) {
							$scope.uploadedImageFlag = true;
							$scope.opacityFlag = true;
							$scope.imageUrl = response.data.Location;
						};
					});
				});
		});




		$('.button-collapse').sideNav('show');
			


			

		};


		
}]);











app.directive('draggable', function() {
  return function(scope, element) {
    // this gives us the native JS object
    var el = element[0];
    
    el.draggable = true;
    
    el.addEventListener(
      'dragstart',
      function(e) {
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('Text', this.id);
        this.classList.add('drag');
        return false;
      },
      false
    );
    
    el.addEventListener(
      'dragend',
      function(e) {
        this.classList.remove('drag');
        return false;
      },
      false
    );
  }
});

app.directive('droppable', function() {
  return {
    scope: {
      drop: '&',
      bin: '='
    },
    link: function(scope, element) {
      // again we need the native object
      var el = element[0];
      
      el.addEventListener(
        'dragover',
        function(e) {
          e.dataTransfer.dropEffect = 'move';
          // allows us to drop
          if (e.preventDefault) e.preventDefault();
          this.classList.add('over');
          return false;
        },
        false
      );
      
      el.addEventListener(
        'dragenter',
        function(e) {
          this.classList.add('over');
          return false;
        },
        false
      );
      
      el.addEventListener(
        'dragleave',
        function(e) {
          this.classList.remove('over');
          return false;
        },
        false
      );
      
      el.addEventListener(
        'drop',
        function(e) {
          // Stops some browsers from redirecting.
          if (e.stopPropagation) e.stopPropagation();
					if(document.getElementById("text")) {
							document.getElementById("text").remove();
					}
          
          this.classList.remove('over');
          
          var binId = this.id;
          var item = document.getElementById(e.dataTransfer.getData('Text')).cloneNode(true);
          item.classList.remove('drag');
					item.id = 'text';
          this.appendChild(item);
          // call the passed drop function
          scope.$apply(function(scope) {
						$('#text').draggable({
							helper: 'clone'
						});
            var fn = scope.drop();
            if ('undefined' !== typeof fn) {            
              fn(item.id, binId);
            }
          });
          
          return false;
        },
        false
      );
    }
  }
});

app.controller('DragDropCtrl', function($scope) {
  $scope.handleDrop = function(item, bin) {
    alert('Item ' + item + ' has been dropped into ' + bin);
  }
});


	var app = angular.module('app', ['ui.router', 'angular-jwt'])
		.config(['$urlRouterProvider','$stateProvider', '$httpProvider',
			function($urlRouterProvider, $stateProvider, $httpProvider) {
				$urlRouterProvider.otherwise('/dashboard');
				$stateProvider
					.state('base', {
						abstract: true,
						url: '',
						templateUrl: 'views/base.html'
					})
					.state('dashboard', {
                        parent: 'base',
                        url: '/dashboard',
                        templateUrl: 'views/dashboard.html',
                        controller: 'dashboardController'
                    })

					.state('adminPanel', {
                        parent: 'base',
                        url: '/admin',
                        templateUrl: 'views/adminPanel.html',
                        controller: 'dashboardController'
                    })
			}]);


			app.controller('appController',['$scope','$rootScope', 'jwtHelper', '$state',function($scope, $rootScope, jwtHelper, $state){
				this.initUser = function(user){
					$rootScope.currentUser = user || {};
					$rootScope.currentUser.token = user.token;
					$rootScope.currentUser.data = jwtHelper.decodeToken($rootScope.currentUser.token);
					console.log($rootScope);
					$rootScope.logout = function() {
						$rootScope = {};
						window.location.href = '/logout';
						console.log('rootscope:', $rootScope);
					}
					$('#projectlist').hide();
				};
			}]);

			app.factory('httpRequestInterceptor', ['$rootScope', function ($rootScope) {
				return {
					request: function ($config) {
						if ($rootScope.currentUser && $rootScope.currentUser.token) {
							$config.headers['x-access-token'] = $rootScope.currentUser.token;
						}
						return $config;
					}
				};
			}]);


            
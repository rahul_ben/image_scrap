
var app = angular.module('app')
app.service('dataService',['ajaxService', function(ajaxService) {
    this.getTopics = function(callback) {
        ajaxService.get(null, '/topic', callback);
    };
    this.getQuotes = function (topicId, p, callback) {
        ajaxService.get(null, '/quote?id='+ topicId + '&page=' + p || 1, callback);
    };
    this.uploadImage = function (data, callback) {
        ajaxService.post(data, '/upload', callback);
    };
}]);
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('fileUpload', ['$http', '$rootScope', function ($http, $rootScope) {
    this.uploadFileToUrl = function(file, uploadUrl, callback){
        var fd = new FormData();
        fd.append('file', file);
        fd.append('user', $rootScope.currentUser.data.email);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(response) {
            callback(response);
        });

    }
}]);
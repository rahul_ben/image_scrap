var app = angular.module('adminApp')
app.service('adminService',['ajaxService', function(ajaxService) {
    this.getUsers = function(callback) {
        ajaxService.get(null, '/api/users', callback);
    };
    
}]);
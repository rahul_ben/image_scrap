var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var User = mongoose.Schema({
    local            : {
        email        : String,
        password     : String,
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    email: {type: String, default: ''},
    name: {type: String, default: ''},
    token: {type: String, default: ''},
    username: {type: String, default: ''},
    images: [{}],
    shortBio: {type: String, default: ''},
    emergency_contact: {type: String, default: ''},
    special_instructions: {type: String, default: ''},
    role: {type: String, default: ''},
    createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now },
    status: { type: Boolean, default : true }
});



User.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});


// methods ======================
// generating a hash
User.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
User.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
mongoose.model('User', User);

var mongoose = require('mongoose');
var mongoose = require('mongoose');

var Quote = mongoose.Schema({
    body: {type: String, default: ''},
    author: {type: String, default: ''},
    topic: {type: mongoose.Schema.Types.ObjectId, ref: 'Topic'},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now },
});



Quote.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});


// create the model for users and expose it to our app
mongoose.model('Quote', Quote);

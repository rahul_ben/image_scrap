var mongoose = require('mongoose');

var Topic = mongoose.Schema({
    name: {type: String, default: ''},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now },
});



Topic.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});


// create the model for users and expose it to our app
mongoose.model('Topic', Topic);

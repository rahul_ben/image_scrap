var express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');
var http = require('http');
var config = require('./settings/config');

var app = express();

require('./settings/database').configure(mongoose);
require('./settings/passport')(passport);
require('./settings/express').configure(app, passport);
require('./settings/routes').configure(app, passport);



var server = http.createServer(app);
var port = process.env.PORT || config.webServer.port || 3000;
server.listen(port, function() {
    console.log('express running on port :-' + port);
});


exports.module = exports = app;
var models = require('mongoose').models;

exports.create = function (req, res) {
    if(!req.body.name) 
        return res.json({error: "name is required"});
    var topic = new models.Topic();
    topic.name = req.body.name;
    topic.save(function(err, newTopic){
        if(err) return res.json({ "error": err});
        res.json({ "id": newTopic.id});
    });
}


exports.all = function (req, res) {
    var mongoQuery = models.Topic.find()
    if(req.query.id){
        mongoQuery.where('topic').equals(req.query.id);
    }
    mongoQuery
        .populate({ path: 'topic', select: 'name' })
        .exec(function(err, topics) {
            if(err) return res.json({ "error": err});
            res.json({items: topics});
        })
}
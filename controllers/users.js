var models = require('mongoose').models;
var formidable = require('formidable');
var AWS = require('aws-sdk');
var fs = require('fs');
var moment = require('moment');
var mailer = require('./../components/mailer');
var config = require('./../settings/config');
AWS.config.credentials = config.awsCredentials;
var s3 = new AWS.S3({
    apiVersion: '2012–09–25'
});

var uploadDoc = function(name, path, callback) {
    var read = fs.createReadStream(path);
     var params = {Bucket: 'quote-image-bucket', Key: name, Body: read};
        s3.upload(params, function(err, data) {
            if(err) return callback (err);
            callback(null, data);
        })
}


exports.upload = function(req, res) {
    var data = {};
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        if(files.file) {
            var splitExt = files.file.name.split('.');
            splitExt[0] = splitExt[0] + '-' + moment().unix().toString();
            files.file.name = splitExt[0] + splitExt[1];
            uploadDoc(files.file.name, files.file.path, function(err, subtitleData) {
                if(err) return res.json({error: err });
                models.User.findOne({
                    "local.email": fields.user
                }, function(err, userData) {
                    if(err) return res.json({error: err });
                    if(!userData) return res.render('login', { message: 'No Such User Exist' }); 
                    var img = {}
                    img.size = files.file.size;
                    img.type = files.file.type;
                    img.name = files.file.name;
                    img.uploadTime = moment().unix();
                    img.link = subtitleData.Location;
                    userData.images.push(img);
                    userData.save(function(err, user) {
                        if(err) return res.json({error: err });
                        res.json(subtitleData)
                    })
                });
                    
                
            });
        } else {
            res.json("upload is required");
        }
        
    });

}

exports.newPassword  = function(req, res) {
    models.User.findOne({
        "local.email": req.body.email
    }, function(err, userData) {
        if(err) return res.json({error: err });
        if(!userData) return res.render('login', { message: 'No Such User Exist' }); 
        userData.local.password = userData.generateHash(req.body.password);;
        userData.save(function(err, user) {
            if(err) return res.json({error: err });
            res.render('login.ejs', { message: 'Ready To Login Now' }); 
        })
        
    })
}

exports.sendVerificationLink  = function(req, res) {
    models.User.findOne({
        "local.email": req.body.email
    }, function(err, userData) {
        if(err) return res.json({error: err });
        if(!userData) return res.render('forgetPassword.ejs', { message: 'No such user exists' }); 
        userData.save(function(err, user) {
            var text = 'Please click to change password';
            var route = '----><a href="' + config.webServer.url + 'newPassword?email='+ req.body.email  + '" + >'+ text + '</a><----' // html body
            mailer.sender(req.body.email, 'Change Password', text, route,  function(err, msgID, mailerResponse) {
                if(err) return res.json({error: err });
                res.render('forgetPassword.ejs', { message: "Please Check Your Email"}); 
            })
            
        })       
    });
}


exports.getUsers  = function(req, res) {
    models.User.find({
        "role": "user"
    }) .exec(function(err, users) {
        if(err) return res.json(err);
        res.json({items: users})
    })    
}
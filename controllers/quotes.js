var enterpreneur = require('./../quotesFiles/enterpreneur.json');
var fitnessJSON = require('./../quotesFiles/fitness.json');
var travelJSON = require('./../quotesFiles/travel.json');
var motivationJSON = require('./../quotesFiles/motivation.json')
var romanticJSON = require('./../quotesFiles/romantic.json')
var lifeJSON = require('./../quotesFiles/life.json')
var models = require('mongoose').models
var async = require('async');

var travelfunction = function() {
    async.eachSeries(fitnessJSON, function(item, next) {
        models.Topic.findOne({ name: "fitnessJSON"})
                    .exec(function(err, topic) {
                        var quote = new models.Quote({
                            // body: item.text,
                            // author: item.from,
                            // topic: topic.id
                            body: item.body,
                            author: item.author,
                            topic: topic.id
                        });
                        quote.save(function(err, newQuote) {
                            if(err) return res.json({ "error": err});
                            next();
                        })
                        
                    })
    }, function() {
        console.log('done')
        res.json({success: "true"})
    })
}


var fitnessfunction = function() {
    async.eachSeries(fitnessJSON, function(item, next) {
        models.Topic.findOne({ name: "fitness"})
                    .exec(function(err, topic) {
                        var quote = new models.Quote({
                            // body: item.text,
                            // author: item.from,
                            // topic: topic.id
                            body: item.body,
                            author: item.author,
                            topic: topic.id
                        });
                        quote.save(function(err, newQuote) {
                            if(err) return res.json({ "error": err});
                            next();
                        })
                        
                    })
    }, function() {
        console.log('done')
    })
}

var enterpreneurfunction = function() {
    async.eachSeries(enterpreneur, function(item, next) {
        models.Topic.findOne({ name: "enterpreneurship"})
                    .exec(function(err, topic) {
                        var quote = new models.Quote({
                            body: item.text,
                            author: item.from,
                            topic: topic.id
                        });
                        quote.save(function(err, newQuote) {
                            if(err) return res.json({ "error": err});
                            next();
                        })
                        
                    })
    }, function() {
        console.log('done')
    })
}

var motivationfunction = function() {
    async.eachSeries(motivationJSON, function(item, next) {
        models.Topic.findOne({ name: "motivation"})
                    .exec(function(err, topic) {
                        var quote = new models.Quote({
                            body: item.quote,
                            author: item.author_name,
                            topic: topic.id
                        });
                        quote.save(function(err, newQuote) {
                            if(err) return res.json({ "error": err});
                            next();
                        })
                        
                    })
    }, function() {
        console.log('done')
    })
}


var lifeFunction = function () {
    async.eachSeries(lifeJSON, function(item, next) {
        models.Topic.findOne({ name: "life"})
                    .exec(function(err, topic) {
                        var quote = new models.Quote({
                            // body: item.text,
                            // author: item.from,
                            // topic: topic.id
                            body: item.body,
                            author: item.author,
                            topic: topic.id
                        });
                        quote.save(function(err, newQuote) {
                            if(err) return res.json({ "error": err});
                            next();
                        })
                        
                    })
    }, function() {
        console.log('done')
        res.json({success: "true"})
    });
}


exports.create = function(req, res) {
      async.eachSeries(lifeJSON, function(item, next) {
        models.Topic.findOne({ name: "life"})
                    .exec(function(err, topic) {
                        var quote = new models.Quote({
                            body: item,
                            topic: topic.id
                        });
                        quote.save(function(err, newQuote) {
                            if(err) return res.json({ "error": err});
                            next();
                        })
                        
                    })
    }, function() {
        console.log('done')
        res.json({success: "true"})
    });
}



exports.all = function(req, res) {
    var perPage = 5;
    var page = req.query.page || 1;
    var mongoQuery = models.Quote.find()
    if(req.query.id){
        mongoQuery.where('topic').equals(req.query.id);
    }
    mongoQuery
        .limit(perPage)
        .skip(perPage * page)
        .populate({ path: 'topic', select: 'name' })
        .exec(function(err, quotes) {
            if(err) return res.json({ "error": err});
            res.json({items: quotes});
        })
}
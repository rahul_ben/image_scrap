const nodemailer = require('nodemailer');
var config = require('./../settings/config')
exports.sender = function(toEmail, subject, text, route, callback) {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.webServer.adminEmail,
            pass: 'Beniwal@123'
        }
    });
    // setup email data with unicode symbols
    let mailOptions = {
        from: config.webServer.adminEmail, // sender address
        to: toEmail, // list of receivers
        subject: subject, // Subject line
        text: text, // plain text body
        html: route
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return callback(error);
        }
        callback(null, info.messageId, info.response);
    });
}